const express = require('express');
const app = express();
const { HOST, PORT } = require('./configuration');
const { connectDB } = require('./utils/db');
const { User } = require('./models/user');

const startServer = () => {
  app.listen(+PORT, () => {
    console.log("API-service is working");
    console.log(`Host is ${HOST}`);
  });
};

const connection = connectDB();
connection.on('error', console.error.bind(console, 'connection error:'));
connection.once('open', startServer);

app.get('/test', (req, res) => {
  res.send('Server is working correctly');
});

app.get('/users', async (req, res) => {
  try {
    const user = new User({firstName: 'Vasyl', lastName: 'Zubrytskyi'});
    await user.save();
    const users = await User.find();
    res.json({ users });
  } catch (err) {
    res.send({ err });
  }
});