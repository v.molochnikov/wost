const monguse = require('mongoose');
const { db } = require('./../configuration');

module.exports.connectDB = () => {
    monguse.connect(db, { useNewUrlParser: true });
    return monguse.connection;
}